package TankGame;

import java.util.Random;

public class Wall {
    
    private int height, y;
    final int x,width;
    private Random r = new Random();
    
    public Wall(int x, int y, int width) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = 480 - this.y;
    }
    
    public static void tankCollision(Tank t, Wall w) {
        int tX = t.getX();
        int wX = w.getX();
        if (tX < wX) {
            if (tX + 30 >= wX) {
                t.setX(wX - 30);
            }
        }
        else {
            if (tX <= wX + w.getWidth()) {
                t.setX(wX + w.getWidth());
            }
        }
    }
    
    public static boolean projectileCollision(Wall w, Projectile p) {
        if (p.getY() > w.getY()) {
        	int pX = p.getX();
            return ((pX > w.getX()) && (pX < w.getX() + w.getWidth()));
        }
        else {
        	return false;
        }
    }
    
    public void setY(int y) {
        this.y = y;
        height = 480 - this.y;
    }
    
    public int getY() {
    	return y;
    }
    
    public int getHeight() {
    	return height;
    }
    
    public int getX() {
        return x;
    }
    
    public int getWidth() {
        return width;
    }
    
    public void randomize() {

		int num = 50 + r.nextInt(330);
		this.setY(num);
    }
}
