package TankGame;

import javax.swing.*;

public class Frame {
    //This is the main method where everything starts. It creates a new board.
    public static void main (String[] arg) throws InterruptedException {
        
        JFrame frame = new JFrame("Tank Game");
        frame.add(new Board());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(640, 480);
        frame.setVisible(true);
    }
}