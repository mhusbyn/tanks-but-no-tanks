package TankGame;
import java.lang.Thread;
import java.lang.InterruptedException;

public class Projectile {
	
	private int PROJECTILEHEIGHT, PROJECTILEWIDTH, v0, x, y, xDirection, yDirection;
	private double gravity, angle, vX, vY;
	private long curTime, lastTime, timeDifference, totalTime;
	private Tank t, tF;
	private Wall w;
	
	//Boolean to see if the projectile is moving, i.e. "active".
	private boolean active = false;
    
    // Method to initialize the constants. Apparently I can't do that outside a method.
    private void initialize() {
		PROJECTILEHEIGHT = 5;
		PROJECTILEWIDTH = 5;
	 
		// Range: 0.008 and up.
		gravity = 0.008;
		v0 = 10;
			
		vX = Math.cos(angle)*v0;
		vY = Math.sin(angle)*v0;
		
		curTime = System.currentTimeMillis();
		lastTime = curTime;
		timeDifference = (curTime - lastTime);
		totalTime = timeDifference;
	}
		       

    public int getX() {
    	return x;
    }
    
    public int getY() {
    	return y;
    }
    
    private void calcMovement() throws InterruptedException {
    	
    	lastTime = curTime;
        curTime = System.currentTimeMillis();
        	
        timeDifference = (curTime - lastTime);
        totalTime += timeDifference;
            
        setYDirection((int) (vY  - (gravity * totalTime)));
        setXDirection((int) vX);

        // Throws exception and shit
        
        Thread.sleep(10);
            	
    }
    public void move()  throws InterruptedException{
    	
        calcMovement();
    	
        x += xDirection;
        y -= yDirection;
        
        // implemented in a different method
        
        //if (x <=0) {
            //x= 0;
        //}
        //if (x >= 640 - PROJECTILEWIDTH) {
            //x = 640 - PROJECTILEWIDTH;
        //}
        //if (y <= PROJECTILEHEIGHT -5) {
           //y = PROJECTILEHEIGHT-5;
        //}
        //if (y >= 480 - PROJECTILEHEIGHT) {
            //y = 480 - PROJECTILEHEIGHT;
        //}
    }
     
    //These assign values to the x and y directions based on which key is pressed and released.
    public void setXDirection(int xdir) {
        xDirection = xdir;
    }
    
    public void setYDirection(int ydir) {
        yDirection = ydir;
    }
  
    
    // Constructor. Could implement initial velocity here if we want to.
    public Projectile (Tank tFriend, Tank tFoe, Wall w) {
    	active = false;
    	
    	this.w = w;
    	
    	t = tFoe;
    	tF = tFriend;
    	
        x = tFriend.getX() + 15 + (int) (tFriend.getBarrelX()) ;
        y = tFriend.getY() + 10 - (int) (tFriend.getBarrelY()) ;
        
        angle = Math.toRadians(tFriend.getA());
        
        initialize();

        
    }
    
   /* public Projectile(int startX,int startY,double angleDeg, Tank t1, Tank t2) {
    	
        x = startX;
        y = startY;
        
        angle = Math.toRadians(angleDeg);
        initialize();
        
        this.activate(t1, t2);

        
    }*/
    
    
    public boolean isActive() {
		return active;
	}
	
	//Method to monitor collisions, both with respect to tanks and the edges of the frame. Also necessary to trigger movement of the projectile.
	
    // Tank that is going to get hit is the argument.
	public void activate()  throws InterruptedException {
		active = true;
		move();
		
		if (this.collision()) {
			t.setHealth(t.getHealth() - 10);
			active = false;
			tF.setTurn(false);
			t.setTurn(true);
			System.out.println("Tank hit.");
			w.randomize();
		}
		else if (x > 640) {
			active = false;
			tF.setTurn(false);
			t.setTurn(true);
			System.out.println("Out of bounds");
			w.randomize();
		}
		else if (y > 480) {
			active = false;
			tF.setTurn(false);
			t.setTurn(true);
			System.out.println("Out of bounds");
			w.randomize();
		}
		else if (Wall.projectileCollision(w, this)) {
			active = false;
			tF.setTurn(false);
			t.setTurn(true);
			w.randomize();
		}
		
	}
	// Needs to be adjusted if we change the shape of the tank.
	private boolean collision() {
		
		boolean inX = (x >= t.getX()) && (x <= t.getX() + 30);
		boolean inY = (y >= t.getY()) && (y <= t.getY() + 20);
		
		return inX && inY;
		
	}
	
	public void setActive(boolean a) {
		active = a;
	}
	
	
	
	
	
	
}
