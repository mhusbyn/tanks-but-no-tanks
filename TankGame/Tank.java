package TankGame;

import java.awt.*;
import java.awt.event.*;


public class Tank{
    private int x, dx, y, health;
    private double a, da;
    private Image still;
    private boolean turn, leftFacing, shot;
    private double barrelx=30;
    private double barrely=0;
	private boolean alive = true;
    
    
    public Tank (int x, int y, boolean leftFacing, double angle) {
        //leftFacing is just so that later the barrel always moves up when you press up etc regardless of which way the tank faces. This doesn't work and might screw up the angle of which the projectile is fired. Martin
        this.x = x;
        this.y = y;
        this.leftFacing = leftFacing;
		this.a = angle;
    	    
        health = 100;
    }
    
    public void move() {
        //move mothod that's called from Board increases the x and a values when keys are pressed
        x += dx;
        a += da;
        
        if (x<0) {
            x = 0;
        }
        if (x>610) {
            x = 610;
        }
    }
    //get methods for returning values 
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public double getA() {
        return a;
    }
    //coordinates of the barrel used in drawing the line
    public void setBarrelX() {
        barrelx = Math.cos(Math.toRadians(a))*30.0;
    } 
    
    public void setBarrelY() {
        barrely = Math.sin(Math.toRadians(a))*30.0;
    }

    public void setTurn(boolean t) {
	turn = t;
    }
    //more get methods
    public double getBarrelX(){
        return barrelx;
    }
    public double getBarrelY(){
        return barrely;
    }
    public Image getImage() {
        return still;
    }
    public boolean getTurn() {
        return turn;
    }
    
    public boolean getShot() {
    	return shot;
    }
    
    public void setShot(boolean shot) {
    	this.shot = shot;
    }
    
    //what happens when keys are pressed
    public void keyPressed(KeyEvent e){
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT) {
            dx = -1;
        }
        if (key == KeyEvent.VK_RIGHT) {
            dx = 1;
        }
        if (key == KeyEvent.VK_UP) {
            if (leftFacing) {
                da = 0.1;
            } else {
                da = -0.1;
            }
            setBarrelX();
            setBarrelY();
        }
        if (key == KeyEvent.VK_DOWN) {
            if (leftFacing) {
                da = -0.1;
            } else {
                da = 0.1;
            }
            setBarrelX();
            setBarrelY();
        }
        if (key == KeyEvent.VK_SPACE) {
        	if (turn) {
        		shot = true;
            	System.out.println("Space pressed" + this);
        	}
        }
            
    }
    public void keyReleased(KeyEvent e){
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT) {
            dx = 0;
        }
        if (key == KeyEvent.VK_RIGHT) {
            dx = 0;
        }
        if (key == KeyEvent.VK_UP) {
            da = 0;
            setBarrelX();
            setBarrelY();
        }
        if (key == KeyEvent.VK_DOWN) {
            da = 0;
            setBarrelX();
            setBarrelY();
        }
        if (key == KeyEvent.VK_SPACE) {
        	//turn = !turn;
        }
                                  
                                  
    }
    
    public int getHealth() {
		return health;
	}
	
	public boolean isAlive() {
		return alive;
	}
	
	public void setHealth(int value) {
		health = value;
		alive = health > 0;
	}
	
	public void setX(int x) {
		this.x = x;
	}
}
          
            
        
    
        
