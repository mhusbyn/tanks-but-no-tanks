package TankGame;
import java.util.Random;

import javax.swing.*;
import java.awt.event.* ;
import java.awt.*;
import java.awt.geom.Line2D;
import java.lang.InterruptedException;

//Board essentially is responisble for painting all of the objects
public class Board extends JPanel implements ActionListener {
    
    //instances of both tanks etc
    Tank tank1;
    Tank tank2;
    // Projectile to be used for shooting.
    Projectile projectile;
    Image bgi;
    Timer time;
    Boolean player2Turn;
    Wall w = new Wall(315, 200, 10);
    Random r = new Random();

    
    public Board() {
        //When Board is called from the main method it creates both tanks and a timer
        tank1 = new Tank(500, 400, false, 180);
        tank2 = new Tank(40, 400, true, 0);
        projectile = new Projectile(tank1, tank2, w);
        tank2.setTurn(true);
        addKeyListener(new AL());
        setFocusable(true);
        ImageIcon i = new ImageIcon(/*place for a background image*/);
        bgi = i.getImage();
        time = new Timer(5, this);
        time.start();
        
    }
    
	public void actionPerformed(ActionEvent e) {
		// Keyboard input controls one tank depending on which ones turn it is.
		// Add projectile if shot == true.
		if (projectile.isActive()) {
			try {
				projectile.activate();
				repaint();
			} catch (InterruptedException f) {
				System.out.println(f);
			}
		} else {
			try {
				if (tank1.getTurn() == true && tank1.isAlive()) {
					Wall.tankCollision(tank1,w);
					tank1.move();					
					if (tank1.getShot() == true) {
						tank1.setShot(false);
						projectile = new Projectile(tank1, tank2, w);
						projectile.activate();

						System.out.println("Tank 1 angle: " + tank1.getA());
						System.out.printf("Tank1 turn: %s\nTank2 turn: %s\n",tank1.getTurn(),tank2.getTurn());
					}
				} else if (tank2.isAlive()){
					Wall.tankCollision(tank2,w);
					tank2.move();
					if (tank2.getShot() == true) {
						tank2.setShot(false);
						projectile = new Projectile(tank2, tank1, w);
						projectile.activate();

						System.out.println("Tank 2 angle: " + tank2.getA());
					}

				}
				repaint();
			} catch (InterruptedException i) {
				System.out.println(i);
			}
		}
	}
    public void paint(Graphics g) {
        
        //this is where all the graphics are done. Coordinates are obtained using get methods from the different classes. (Currently just the tank)
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        
        
        // Health bar for tank1
        g2d.drawRect(10, 15, 100, 20);
        
        // Health bar for tank2
        g2d.drawRect(520, 15, 100, 20);
        
        g2d.setColor(Color.green);
        
        g2d.fillRect(10,15, tank2.getHealth(), 20);
        g2d.fillRect(520, 15, tank1.getHealth(), 20);
        
        g2d.setColor(Color.black);
        
        //Draw wall.
        g2d.fillRect(w.getX(), w.getY(), w.getWidth(), w.getHeight());
        
        g2d.drawImage(bgi, 0, 0, null);
        g2d.fillRect(tank1.getX(), tank1.getY(), 30, 20);//main tank body
        g2d.draw(new Line2D.Double (tank1.getX()+15, tank1.getY()+10, tank1.getX()+15+tank1.getBarrelX(), tank1.getY()+10-tank1.getBarrelY()));//barrel
        
        if (projectile.isActive() == true) {
        	g2d.fillOval(projectile.getX(), projectile.getY(), 5, 5);
        }
        // paint projectile if projectile is active.
         //   Projectile projectile1 = new Projectile(tank1.getX()+20, tank1.getY()+20, tank1.getA());
          //  g2d.fillOval(projectile1.getX(), projectile1.getY(), 5, 5);
        
        g2d.fillRect (tank2.getX(), tank2.getY(), 30, 20);
        g2d.draw(new Line2D.Double (tank2.getX()+15.0, tank2.getY()+10.0, tank2.getX()+15.0+tank2.getBarrelX(), tank2.getY()+10.0-tank2.getBarrelY()));
    }
    
    private class AL extends KeyAdapter{
        //When a key is pressed the information is sent to both tanks
        public void keyReleased(KeyEvent e){
            tank1.keyReleased(e);
            tank2.keyReleased(e);
        }
        public void keyPressed(KeyEvent e) {
            tank1.keyPressed(e);
            tank2.keyPressed(e);
        }
        
    }
    
        
}
